##  Daily Summary 

**Time: 2023/7/13**

**Author: 赖世龙**

---

**O (Objective):**    Today, I learned about unit testing and TDD. There are plenty of TDD exercises in class, and unit tests are actually practiced in TDD exercises 

**R (Reflective):**  Satisfied

**I (Interpretive):**   The use of TDD thinking can be better to develop, clear requirements and steps to develop, while echoing the previous learned Tasking 

**D (Decisional):**    I will use TDD thinking to develop and improve the quality of my code and reduce the errors of my code.

