package com.afs.tdd;

public class MarsRover {

    private Location location;

    public MarsRover(Location location) {
        this.location = location;
    }

    public void executeCommand(Command command) {
        if (command == Command.Move) {
            executeCommandMove();
        } else if (command == Command.Left) {
            executeCommandLeft();
        } else if (command == Command.Right) {
            executeCommandRight();
        }
    }

    private void executeCommandRight() {
        if (this.location.getDirection() == Direction.West) this.location.setDirection(Direction.North);
        else if (this.location.getDirection() == Direction.North) this.location.setDirection(Direction.East);
        else if (this.location.getDirection() == Direction.East) this.location.setDirection(Direction.South);
        else if (this.location.getDirection() == Direction.South) this.location.setDirection(Direction.West);
    }

    private void executeCommandMove() {
        int x = this.location.getX();
        int y = this.location.getY();
        if (this.location.getDirection() == Direction.North) this.location.setY(y + 1);
        else if (this.location.getDirection() == Direction.South) this.location.setY(y - 1);
        else if (this.location.getDirection() == Direction.East) this.location.setX(x + 1);
        else if (this.location.getDirection() == Direction.West) this.location.setX(x - 1);
    }

    private void executeCommandLeft() {
        if (this.location.getDirection() == Direction.North) this.location.setDirection(Direction.West);
        else if (this.location.getDirection() == Direction.West) this.location.setDirection(Direction.South);
        else if (this.location.getDirection() == Direction.South) this.location.setDirection(Direction.East);
        else if (this.location.getDirection() == Direction.East) this.location.setDirection(Direction.North);
    }
}
