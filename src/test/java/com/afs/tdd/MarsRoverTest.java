package com.afs.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class MarsRoverTest {

    @Test
    void should_change_location_to_0_1_N_when_execute_given_location_and_move() {
        // given
        Location location = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(location);

        // when
        marsRover.executeCommand(Command.Move);

        // then
        Assertions.assertEquals(new Location(0, 1, Direction.North), location);
    }

    @Test
    void should_change_location_to_0_1_S_when_execute_given_location_and_move() {
        // given
        Location location = new Location(0, 2, Direction.South);
        MarsRover marsRover = new MarsRover(location);

        // when
        marsRover.executeCommand(Command.Move);

        // then
        Assertions.assertEquals(new Location(0, 1, Direction.South), location);
    }

    @Test
    void should_change_location_to_0_0_E_when_execute_given_location_and_move() {
        // given
        Location location = new Location(-1, 0, Direction.East);
        MarsRover marsRover = new MarsRover(location);

        // when
        marsRover.executeCommand(Command.Move);

        // then
        Assertions.assertEquals(new Location(0, 0, Direction.East), location);
    }

    @Test
    void should_change_location_to_0_0_W_when_execute_given_location_and_move() {
        // given
        Location location = new Location(1, 0, Direction.West);
        MarsRover marsRover = new MarsRover(location);

        // when
        marsRover.executeCommand(Command.Move);

        // then
        Assertions.assertEquals(new Location(0, 0, Direction.West), location);
    }

    @Test
    void should_change_location_to_0_0_W_when_execute_given_location_and_left() {
        // given
        Location location = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(location);

        // when
        marsRover.executeCommand(Command.Left);

        // then
        Assertions.assertEquals(new Location(0, 0, Direction.West), location);
    }

    @Test
    void should_change_location_to_0_0_S_when_execute_given_location_and_left() {
        // given
        Location location = new Location(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(location);

        // when
        marsRover.executeCommand(Command.Left);

        // then
        Assertions.assertEquals(new Location(0, 0, Direction.South), location);
    }

    @Test
    void should_change_location_to_0_0_E_when_execute_given_location_and_left() {
        // given
        Location location = new Location(0, 0, Direction.South);
        MarsRover marsRover = new MarsRover(location);

        // when
        marsRover.executeCommand(Command.Left);

        // then
        Assertions.assertEquals(new Location(0, 0, Direction.East), location);
    }

    @Test
    void should_change_location_to_0_0_N_when_execute_given_location_and_left() {
        // given
        Location location = new Location(0, 0, Direction.East);
        MarsRover marsRover = new MarsRover(location);

        // when
        marsRover.executeCommand(Command.Left);

        // then
        Assertions.assertEquals(new Location(0, 0, Direction.North), location);
    }

    @Test
    void should_change_location_to_0_0_N_when_execute_given_location_and_right() {
        // given
        Location location = new Location(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(location);

        // when
        marsRover.executeCommand(Command.Right);

        // then
        Assertions.assertEquals(new Location(0, 0, Direction.North), location);
    }

    @Test
    void should_change_location_to_0_0_E_when_execute_given_location_and_right() {
        // given
        Location location = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(location);

        // when
        marsRover.executeCommand(Command.Right);

        // then
        Assertions.assertEquals(new Location(0, 0, Direction.East), location);
    }

    @Test
    void should_change_location_to_0_0_S_when_execute_given_location_and_right() {
        // given
        Location location = new Location(0, 0, Direction.East);
        MarsRover marsRover = new MarsRover(location);

        // when
        marsRover.executeCommand(Command.Right);

        // then
        Assertions.assertEquals(new Location(0, 0, Direction.South), location);
    }

    @Test
    void should_change_location_to_0_0_W_when_execute_given_location_and_right() {
        // given
        Location location = new Location(0, 0, Direction.South);
        MarsRover marsRover = new MarsRover(location);

        // when
        marsRover.executeCommand(Command.Right);

        // then
        Assertions.assertEquals(new Location(0, 0, Direction.West), location);
    }

    @Test
    void should_change_location_to_4_1_S_when_execute_given_location_and_batch_command() {
        // given
        Location location = new Location(3, 3, Direction.South);
        MarsRover marsRover = new MarsRover(location);

        // when
        marsRover.executeCommand(Command.Move);
        marsRover.executeCommand(Command.Left);
        marsRover.executeCommand(Command.Move);
        marsRover.executeCommand(Command.Right);
        marsRover.executeCommand(Command.Move);

        // then
        Assertions.assertEquals(new Location(4, 1, Direction.South), location);
    }
}
